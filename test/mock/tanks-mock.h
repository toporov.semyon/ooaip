#include "factories.cpp"
#include "gmock/gmock.h"

template<class Vector>
class MockMovable : public Movable <Vector>{
public:
    MOCK_METHOD(Vector, getPosition, (), (const, override));
    MOCK_METHOD(void, setPosition, (Vector const &newValue), (override));
    MOCK_METHOD(Vector, getVelocity, (), (const, override));
};

template<class Vector>
class MockOilable : public Fuelable <Vector>{
public:
    MOCK_METHOD(Vector, getFuelLevel, (), (const, override));
    MOCK_METHOD(void, setFuelLevel, (Vector const &newValue), (override));
};

template<class Vector>
class MockMoveOilable : public Movable <Vector>, public Oilable <Vector> {
public:
    MOCK_METHOD(Vector, getPosition, (), (const, override));
    MOCK_METHOD(void, setPosition, (Vector const &newValue), (override));
    MOCK_METHOD(Vector, getVelocity, (), (const, override));
    MOCK_METHOD(Vector, getOil, (), (const, override));
    MOCK_METHOD(void, setOil, (Vector const &newValue), (override));
};
