/*
#include "mock/tanks-mock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <queue>
#include <map>

//using ::testing::AtLeast;

ACTION(ThrowReadPropertyException) {
    throw ReadPropertyException();
}

ACTION(ThrowChangePropertyException) {
    throw ChangePropertyException();
}

TEST(MoveCommand, normal) {
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    std::vector<int> velocity = {2, 2};
    std::vector<int> to_position = {4,4};
    EXPECT_CALL(movable, getPosition()).WillOnce(testing::Return(position));
    EXPECT_CALL(movable, getVelocity()).WillOnce(testing::Return(velocity));
    EXPECT_CALL(movable, setPosition(to_position)).WillOnce(testing::Return());
    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);
    move.Execute();
}

TEST(MoveCommand, wrong_len_vector) {
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2, 2};
    EXPECT_CALL(movable, getPosition()).WillOnce(testing::Return(position));

    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);
    EXPECT_THROW(move.Execute(), CommandException);
}

TEST(MoveCommand, wrong_getPosition) {
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    EXPECT_CALL(movable, getPosition()).WillRepeatedly(testing::Throw(ReadPropertyException()));

    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);
    EXPECT_THROW(move.Execute(), ReadPropertyException);
}

TEST(MoveCommand, wrong_setPosition) {
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    std::vector<int> to_position = {4,4};
    EXPECT_CALL(movable, getPosition()).WillOnce(testing::Return(position));
    EXPECT_CALL(movable, setPosition(to_position)).WillRepeatedly(testing::Throw(ChangePropertyException()));

    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);
    EXPECT_THROW(move.Execute(), ChangePropertyException);
}

TEST(MoveCommand, wrong_getVelocity) {
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    std::vector<int> velocity = {2, 2};
    EXPECT_CALL(movable, getPosition()).WillOnce(testing::Return(position));
    EXPECT_CALL(movable, getVelocity()).WillRepeatedly(testing::Throw(ReadPropertyException()));

    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);
    EXPECT_THROW(move.Execute(), ReadPropertyException);
}

TEST(Gamequeue, normal) {

    GameCommands gameQueue = GameCommands();
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    std::vector<int> velocity = {2, 2};
    std::vector<int> to_position = {4,4};
    EXPECT_CALL(movable, getPosition()).WillRepeatedly(testing::Return(position));
    EXPECT_CALL(movable, getVelocity()).WillRepeatedly(testing::Return(velocity));
    EXPECT_CALL(movable, setPosition(to_position)).WillRepeatedly(testing::Return());
    std::vector<int> v = {2, 2};
    MoveCommand<std::vector<int> > move = MoveCommand<std::vector<int> >(movable, v);

    std::vector<int> oil = {5};
    std::vector<int> oil2 = {4};
    MockOilable<std::vector<int> > oilable;
    EXPECT_CALL(oilable, getOil()).WillRepeatedly(testing::Return(oil));
    EXPECT_CALL(oilable, setOil(oil2)).WillRepeatedly(testing::Return());
    CheckOilCommand<std::vector<int> > checkOil = CheckOilCommand<std::vector<int> >(oilable);
    BurnOilCommand<std::vector<int> > burnOil = BurnOilCommand<std::vector<int> >(oilable);
    
    std::queue<Command*> que;
    que.push(&move);
    que.push(&checkOil);
    que.push(&burnOil);
    MacroCommand* macro = new MacroCommand(que);
    RepeatCommand* repeat = new RepeatCommand(gameQueue, dynamic_cast<Command*>(macro), 2);

    gameQueue.AddCommand(macro);
    gameQueue.AddCommand(repeat);
}


TEST(CommandFactory, normal){
    MockMovable<std::vector<int> > movable;
    std::vector<int> position = {2, 2};
    std::vector<int> velocity = {2, 2};
    std::vector<int> to_position = {4,4};
    EXPECT_CALL(movable, getPosition()).WillRepeatedly(testing::Return(position));
    EXPECT_CALL(movable, getVelocity()).WillRepeatedly(testing::Return(velocity));
    EXPECT_CALL(movable, setPosition(to_position)).WillRepeatedly(testing::Return());
    std::vector<int> v = {2, 2};

    std::vector<int> oil = {5};
    std::vector<int> oil2 = {4};
    MockOilable<std::vector<int> > oilable;
    EXPECT_CALL(oilable, getOil()).WillRepeatedly(testing::Return(oil));
    EXPECT_CALL(oilable, setOil(oil2)).WillRepeatedly(testing::Return());

    std::map<std::string, Object<std::vector<int>>> list;
    list["Movable"] = movable;
    list["Oilable"] = oilable;
    UObject<std::vector<int>> object = UObject<std::vector<int>>(list);
    CommandFactory<std::vector<int> > commandFactory = CommandFactory<std::vector<int> >(object, v);
    auto* moveWithOilCommand = commandFactory.CreateMoveWithOilCommand();
    moveWithOilCommand->Execute();

}*/