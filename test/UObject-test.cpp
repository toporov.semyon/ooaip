#include "gtest/gtest.h"

#include "IUObject.h"

TEST(UObject, normal)
{
    UObject tank;
    int *number1_ptr = new int(100);
    int *number2_ptr = new int(200);

    tank["number1_ptr"] = number1_ptr;
    tank["number2_ptr"] = number2_ptr;

    ASSERT_EQ(std::any_cast<int*>(tank["number1_ptr"]), number1_ptr);
    ASSERT_EQ(std::any_cast<int*>(tank["number2_ptr"]), number2_ptr);

    *number1_ptr +=2;
    ASSERT_EQ(*std::any_cast<int*>(tank["number1_ptr"]), 102);
}